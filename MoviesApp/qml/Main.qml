import Felgo 3.0
import QtQuick 2.0
import "model"
import "logic"
import "pages"


App {

    Component.onCompleted: {
        // if device has network connection, clear cache at startup
        // you'll probably implement a more intelligent cache cleanup for your app
        // e.g. to only clear the items that aren't required regularly


        if(isOnline) {
            logic.clearCache()
        }

        // fetch todo list data
//      logic.loginFetchMovies(email,password)

    }


    // business logic
    Logic {
        id: logic
    }

    // model
    DataModel {
        id: dataModel
        dispatcher: logic // data model handles actions sent by logic

//        // global error handling
//        onLoginFetchMoviesFailed: nativeUtils.displayMessageBox("Unable to load movies", error, 1)
//        onFetchMovieDetailsFailed: nativeUtils.displayMessageBox("Unable to load movie "+movie_id, error, 1)
//        onDeleteMovieFailed: nativeUtils.displayMessageBox("Unable to load movie "+movie_id, error, 1)
//        onStoreTodoFailed: nativeUtils.displayMessageBox("Failed to store "+viewHelper.formatTitle(todo))

    }



    // views
    Navigation {
        navigationMode: navigationModeTabs
         id: navigation
        // login page below overlays navigation then
        enabled: dataModel.userLoggedIn

        // first tab
        NavigationItem {
            id: homePageItem
//            title: qsTr("Catalogue")
            icon: IconType.home


            NavigationStack {
//                splitView: tablet // use side-by-side view on tablets
                initialPage: HomePage { }
            }
        }


        NavigationItem {
              id: searchPageItem
              title: qsTr("Add")
              icon: IconType.plus

              NavigationStack{
                  leftColumnIndex: 1
                  splitView: tablet

                  SearchPage { }
              }
        }


        //        testuser2@example.com
        //         124testpassword
        // second tab
        NavigationItem {
            title: qsTr("logout") // use qsTr for strings you want to translate
            icon: IconType.user

            NavigationStack {
                initialPage: ProfilePage {
                    // handle logout
                    onLogoutClicked: {
                        logic.logout()

                        // jump to main page after logout
                        navigation.currentIndex = 0
                        navigation.currentNavigationItem.navigationStack.popAllExceptFirst()
                    }
                }
            }
        }


    }

//     login page lies on top of previous items and overlays if user is not logged in
    LoginPage {
        visible: opacity > 0
        enabled: visible
        opacity: dataModel.userLoggedIn ? 0 : 1 // hide if user is logged in

        Behavior on opacity { NumberAnimation { duration: 250 } } // page fade in/out
    }


}
