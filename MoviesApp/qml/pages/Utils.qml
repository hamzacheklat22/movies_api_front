import QtQuick 2.0
import Felgo 3.0

Item {

    // function to control the screen resolution
    function  widths(){

          if (Window.width <400)
              {
            return page.width / 2
          }
          else if  (Window.width >= 400 && Window.width < 600)
          {
             return page.width / 3
          }
          else if  (Window.width >= 600 && Window.width < 800)
          {
             return page.width / 4
          }
          else if  (Window.width >= 800 && Window.width < 1200)
          {
             return page.width / 5
          }
          else if  (Window.width > 1200)
          {
             return page.width / 10
          }
      }
}
