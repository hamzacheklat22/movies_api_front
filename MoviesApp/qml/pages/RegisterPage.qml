import Felgo 3.0
import QtQuick 2.8
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15



Page {
    id: registerPage
    navigationBarTranslucency: 1
    useSafeArea: false

    Rectangle {
      z: 1
      width: parent.width
      height: Theme.statusBarHeight
      color: "black"
      opacity: 1
    }


    LinearGradient {
      anchors.fill: parent

      start: Qt.point(0, 0)
      end: Qt.point(registerPage.width * 0.2, registerPage.width * 0.7)
      gradient: Gradient {
        GradientStop { position: 0.0; color: "#404040" }
        GradientStop { position: 0.7; color: "#121212" }
      }
    }

    // login form background
    Rectangle {
        id: registerForm
        anchors.centerIn: parent
        color: "white"
        opacity: 1
        width: (registerContent.width)*1.3//registerContent.width + dp(48)
        height: (registerContent.height)*1.3//registerContent.height + dp(16)
        radius: dp(4)


    }

    // login form content
    GridLayout {
        id: registerContent
        anchors.centerIn: registerForm
        columnSpacing: dp((registerPage.width)*0.005)
        rowSpacing: dp(10)
        columns: 2
        width: (registerPage.width)*0.6
        height: (registerPage.height)*0.6

        // headline
        AppText {
            font.bold: true
            fontSize: dp(20)
            Layout.topMargin: dp(8)
            Layout.bottomMargin: dp(12)
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignHCenter
            text: "Register"
        }

        // user name text and field
        AppText {
            text: qsTr("User")
            //font.pixelSize: sp(40)
            fontSize: dp((registerPage.width)*0.01)+dp(8)

        }

        AppTextField {
            id: txtRegisterUserName
            Layout.preferredWidth: (registerForm.width)*0.5
            Layout.preferredHeight: (registerForm.height)*0.07
            showClearButton: true
            font.pixelSize: dp((registerPage.width)*0.01)+dp(8)
            borderColor: 'black'
            borderWidth: !Theme.isAndroid ? dp(2) : 0
        }



        // email text and field
        AppText {
            text: qsTr("E-mail")
            //font.pixelSize: sp(40)
            fontSize: dp((registerPage.width)*0.01)+dp(8)


        }

        AppTextField {
            id: txtRegisterEmail
            Layout.preferredWidth: (registerForm.width)*0.5//dp(150)
            Layout.preferredHeight: (registerForm.height)*0.07
            showClearButton: true
            font.pixelSize: dp((registerPage.width)*0.01)+dp(8)
            borderColor: 'black'
            borderWidth: !Theme.isAndroid ? dp(2) : 0
        }

        // password text and field
        AppText {
            text: qsTr("Password")
            //font.pixelSize: sp(7)
            fontSize: dp((registerPage.width)*0.01)+dp(8)


        }

        AppTextField {
            id: txtRegisterPassword
            Layout.preferredWidth: (registerForm.width)*0.5
            Layout.preferredHeight: (registerForm.height)*0.07
            showClearButton: true
            font.pixelSize: dp((registerPage.width)*0.01)+dp(8)
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            echoMode: TextInput.Password
            borderColor: 'black'
        }

        Column {
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.topMargin: dp(3)
            width: registerForm.width
            // buttons
            AppButton {
                backgroundColor: 'red'
                text: qsTr("Register")
                textSize: dp((registerPage.width)*0.01)+dp(8)
                flat: false
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: {
                    registerPage.forceActiveFocus() // move focus away from text fields

                    // call login action
                    logic.register(txtRegisterUserName.text, txtRegisterEmail.text, txtRegisterPassword.text)
                    delay(500, function() {
                         //navigation.currentNavigationItem.navigationStack.push(loginPage)
                    })

                }


            }
            AppButton {
                text: qsTr("Already have an account?")
                textSize: 12
                flat: true
                //backgroundColor: 'red'
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: {
                    registerPage.forceActiveFocus() // move focus away from text fields

                    // call your logic action to register here
                    console.debug("registering...")
                    //loginPage.navigationStack.push(registerPageComponent)

//                   register()
                }
            }


    }


}
    // function to control the screen resolution
    function  widths(idPage){

          if (Window.width <400)
              {
            return (idPage.width)*0.8
          }
          else if  (Window.width >= 400 && Window.width < 600)
          {
             return (idPage.width)*0.7
          }
          else if  (Window.width >= 600 && Window.width < 800)
          {
             return (idPage.width)*0.6
          }
          else if  (Window.width >= 800 && Window.width < 1200)
          {
             return (idPage.width)*0.5
          }
          else if  (Window.width > 1200)
          {
             return idPage.width / 10
          }
      }

    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }




}
