import Felgo 3.0
import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15

Page {
  id: root
   title: 'detail movie'

    LinearGradient {
      anchors.fill: parent
      start: Qt.point(0, 0)
      end: Qt.point(homePageItem.width * 0.2, homePageItem.width * 0.7)
      gradient: Gradient {
        GradientStop { position: 0.0; color: "#404040" }
        GradientStop { position: 0.7; color: "#121212" }
      }
    }


    // target id
    property string movie_id
    // data property for page
    property var movieData: dataModel.movie_Details[movie_id]
    onMovie_idChanged:  logic.fetchMovieDetails(movie_id)
    backNavigationEnabled: true


     AppFlickable {
       id:detaiFlickable
       anchors {
         fill: parent
       }

       contentHeight: contentColumn.height + contentColumn.anchors.topMargin


       Column {
         id: contentColumn

         anchors {
           top: parent.top
           topMargin: dp(60)
           horizontalCenter: parent.horizontalCenter
         }

         spacing: dp(15)

         AppButton {
           anchors.horizontalCenter: parent.horizontalCenter
           height: dp(40)
           verticalPadding: dp(12)
           horizontalPadding: dp(45)
           radius: height / 2
           backgroundColor: Theme.tintColor
           textColor: Theme.textColor
           flat: false
           text: "Do you want to delete this movie ?"
           textSize: sp(15)


           onClicked: {
               logic.deleteMovie(movie_id)

               delay(500, function() {
                    logic.fetchMovies()
                    })

               delay(500, function() {
                    navigation.currentNavigationItem.navigationStack.popAllExceptFirst()
                    })
           }


           Timer {
               id: timer
           }
           // function delay
           function delay(delayTime, cb) {
               timer.interval = delayTime;
               timer.repeat = false;
               timer.triggered.connect(cb);
               timer.start();
           }

         }


         AppImage {
           anchors.horizontalCenter: parent.horizontalCenter

           width: dp(150)
           height: width
           source: movieData['movie']['poster'] ? movieData['movie']['poster']: ''

         }

         AppText {
           anchors.horizontalCenter: parent.horizontalCenter

           font {
             bold: true
             pixelSize: sp(25)

           }

           text: movieData['movie']['title'] ? movieData['movie']['title']: ''
            color: 'white'
         }

         AppText {
           anchors.horizontalCenter: parent.horizontalCenter

           font {
             bold: true
             pixelSize: sp(25)

           }

           text:  movieData['movie']['runtime'] ? 'Run time:'+ movieData['movie']['runtime']: ''
            color: 'white'
         }

         AppText {
           anchors.horizontalCenter: parent.horizontalCenter
           text: movieData['movie']['synopsis'][0] ?  movieData['movie']['synopsis'][0]: ''
           color: 'white'
           wrapMode : Text.WordWrap
           horizontalAlignment: Text.AlignHCenter
           font.pointSize:sp(5)
           width: dp(180)
         }

        }

       }
   }
