import Felgo 3.0
import QtQuick 2.8
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15
Page {
    title: qsTr("Profile")

    LinearGradient {
      anchors.fill: parent
      start: Qt.point(0, 0)
      end: Qt.point(homePageItem.width * 0.2, homePageItem.width * 0.7)
      gradient: Gradient {
        GradientStop { position: 0.0; color: "#404040" }
        GradientStop { position: 0.7; color: "#121212" }
      }
    }

    signal logoutClicked

    AppButton {
        anchors.centerIn: parent
        text: qsTr("Logout")
        onClicked: logoutClicked()
    }
}
