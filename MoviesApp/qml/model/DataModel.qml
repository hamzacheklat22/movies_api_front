import QtQuick 2.0
import Felgo 3.0

Item {

    // property to configure target dispatcher / logic
    property alias dispatcher: logicConnection.target
    // Input usernama/password
    property var email: _.email
    property var username: _.username
    property var password: _.password
    property var titleMovie: _.titleMovie

    // whether a user is logged in
    readonly property bool userLoggedIn: _.userLoggedIn
    // model data properties
    readonly property alias movies: _.movies
    readonly property alias searchMovies: _.searchMovies
    readonly property alias movie_Details: _.movie_Details
    readonly property alias usertoken: _.usertoken
    readonly property alias movie_id: _.movie_id


    // action error signals
    signal loginFetchMoviesFailed(var error)
    signal fetchMovieDetailsFailed(string movie_id, var error)
    signal deleteMovieFailed(string movie_id, var error)



    Connections {
        id: logicConnection

        // Action register
       onRegister: {
           console.log("logic register activate")
           api.register({username : username,
                         email : email,
                         password : password},
                        function(data) {
                            token = data.body.token
                             if (data.status === 200){
                                 _.userLoggedIn = true
                             }
                        },
                        function(err) {

                        })
     }

        // Action Login and fetch movies catalogue
        onLoginFetchMovies: {
            console.log("logic login activate")
            // check cached value first
            var cached = cache.getValue("movies")
                if(cached)
                     _.movies = cached
            api.loginFetchMovies({email : email, password : password},
                                 function(data) {
                                     _.movies = data.res.body
                                     _.usertoken = data.token
                                     cache.setValue("movies", movies)
                                     if (data.res.status === 200){
                                         _.userLoggedIn = true
                                     }
                                 })
      }


        // Action Logout
        onLogout: {
            api.logout(usertoken,
                       function(data) {
                           if (data.status === 200){
                               _.userLoggedIn = false
                           }
                       })
        }


        // Action Fetch Movies Details
        onFetchMovieDetails: {
            // check cached movies details first
            var cached = cache.getValue("movie_" + movie_id)
            if(cached) {
                _.movie_Details[movie_id] = cached
            }
            // load from api
            else{
                api.getDetailMovies(usertoken, movie_id,
                                    function(data) {
                                        // cache data first
                                        cache.setValue("movie_"+movie_id, data.body)
                                        _.movie_Details[movie_id] = data.body
                                    },
                                    function(error) {
                                        // action failed if no cached data
                                        if(!cached) {
                                            fetchMovieDetailsFailed(movie_id, error)
                                        }
                                    })

            }
        }


        // Action Delete movies
        onDeleteMovie: {
            api.delMovie(usertoken, movie_id,
                         function(data) {
                             if (data.status === 200){
                              console.log(data.body)
                             }
                         })
        }


        // Action display catalog after deleted movie
        onFetchMovies: {
            api.fetchMovies(usertoken,function(data){
                _.movies = data.body

            },function(err){})
        }


        // Action search movies
//        onSearchMovies: {
//            api.search(usertoken,
//                           titleMovie,function(data){
//                _.searchMovies = data.body

//            },function(err){})

//        }


        // Action search movies an loading detail
        onSearchMoviesFetchDetail: {
            api.serachFetchDetail(usertoken, titleMovie,
                                  function(data){
                                      _.searchMovies = data.res.body
                                  },
                                  function(err){})
        }

        // Action Fetch Movies Details
        onFetchMovieDetailsSearch: {
            // check cached movies details first
            var cached = cache.getValue("moviesearch_" + movie_id)
            _.movie_Details[movie_id] = cached
        }


        // Action Add movies
        onAddMovie: {
            console.debug("add logic activate")
            console.debug(movie_id)
            api.addMovie(usertoken, movie_id,
                      function(data) {
                          console.log(data.status)
                          if (data.status === 200){
                              console.log(data.body)
                          }

                 })
      }













  }

  MoviesAPI {
      id: api
      maxRequestTimeout: 5000 // use max request timeout of 5 sec
    }

  // storage for caching
  Storage {
      id: cache
  }

    // private
    Item {
        id: _

        // data properties
        property var movies: []  // Array
        property var movie_Details: ({}) // Map
        property bool userLoggedIn: false
        property string usertoken: ''
        property string movie_id
//        property string title_movie: ''
        property var searchMovies: []  // Array
    }
}

